# User interface for managing drones

This interface connects to http://drone.api.gouv.fr and permits registering and managing one's drones.

There is no installation, dependencies nor compilation to run this project.


## Running

1. Download the sources
2. Run a static server (ie: `python -m SimpleHTTPServer 8080`)
3. Open a browser at http://localhost:8080


## Compatibility

This project is based on JavaScript ES6 syntax.
Most browsers supporting ES6 should probably be able to run this platform:

- Firefox 45 and above
- Chromium 42 and above
- Safari 10 and above
- Edge 13 and above
- Opera 43 and above
