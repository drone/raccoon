'use strict'

class Notifier {
  constructor (options) {
    const defaults = {
      defaultClass: '',
      classTypePrepend: ''
    }
    this.options = Object.assign({}, defaults, options)

    this.container = document.createElement('div')
    this.container.id = 'notifier'
    document.body.appendChild(this.container)
  }

  notify (type, message, delay) {
    this.container.classList = this.options.defaultClass + ' visible'
    this.container.classList.add(this.options.classTypePrepend + type)
    this.container.innerHTML = message
    setTimeout(_ => {
      this.container.classList.remove('visible')
    }, delay || 5000)
    return this.container
  }
}
