'use strict'

class TemplateElement extends HTMLElement {
  constructor () {
    super()
    this.attachShadow({mode: 'open'})
    console.debug('constructing', this.constructor.name)
    this.props = {}
    // get the `document` injected from static `define()` registerer.
    const staticClass = eval(this.constructor.name)
    this.document = staticClass.document
    this.elementName = staticClass.elementName
  }

  filterIf (html) {
    const wrapper = document.createElement('div')
    wrapper.innerHTML = html
    ;[].forEach.call(wrapper.querySelectorAll('[if=false]'), node => node.remove())
    return wrapper.innerHTML
  }

  get template () {
    const template = this.document && this.document.querySelector(`template[name=${this.elementName}]`)
    if (!template) return ''
    const templateContent = template.innerHTML.replace(/&quot;/g, '"')
    const replaceTemplate = new Function(`return \`${templateContent}\``)
    const content = replaceTemplate.call(this)
    return this.filterIf(content)
  }

  get style () {
    const style = this.document && this.document.querySelector(`style[name=${this.elementName}]`)
    if (!style) return ''
    this.shadowRoot.appendChild(style.cloneNode(true))
    const styleContent = style.outerHTML.replace(/&quot;/g, '"')
    const replaceStyle = new Function(`return \`${styleContent}\``)
    return replaceStyle.call(this)
  }

  connectedCallback () {
    this.render()
    this.complete()
  }

  render () {
    this.shadowRoot.innerHTML = this.style + this.template
  }

  redraw () {
    this.shadowRoot.innerHTML = this.shadowRoot.innerHTML
  }

  complete () {}

  static define (elementName, classRef) {
    // Dynamic creation on an element from JS means that document refers to then
    // calling script, not this element. Thus we need to save the reference
    // when registering the component.
    classRef.document = document.currentScript.ownerDocument
    classRef.elementName = elementName
    customElements.define(elementName, classRef)
  }
}
