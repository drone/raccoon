'use strict'

class User {

  constructor () {
    this.id = localStorage.getItem('userid')
    this.username = localStorage.getItem('username')
  }

  get isAuthenticated () {
    return Boolean(this.id)
  }

  checkAuthenticated () {
    return api.get('/me/')
      .then(data => this.authenticate(data))
      .catch(error => {
        this.logout()
        if (error.status !== 403) console.error(error)
        throw new Error(error)
      })
  }

  authenticate (data) {
    this.id = data.id
    this.username = data.username || data.fullname
    localStorage.setItem('userid', this.id)
    localStorage.setItem('username', this.username)
    return this
  }

  logout () {
    this.id = null
    this.username = null
    localStorage.removeItem('userid')
    localStorage.removeItem('username')
    return this
  }

}
