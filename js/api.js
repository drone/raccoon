'use strict'
class ServerError extends Error {}

class Api {
  constructor (root) {
    this.root = root
  }

  get (endpoint) {
    const url = `${this.root}${endpoint}`
    const options = {
      mode: 'cors',
      credentials: 'include'
    }
    return request(url, options)
  }

  put (endpoint, data) {
    const url = `${this.root}${endpoint}`
    const options = {
      method: 'PUT',
      body: data,
      mode: 'cors',
      credentials: 'include'
    }
    return request(url, options)
  }
}
