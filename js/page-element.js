class PageElement extends TemplateElement {
  static get observedAttributes () {
    return ['hash']
  }

  attributeChangedCallback (name, oldValue, value) {
    if (name === 'hash') {
      this.moveToHash()
    }
  }

  render () {
    super.render()
    if (this.getAttribute('hash')) {
      this.moveToHash()
    }
  }

  moveToHash () {
    const hash = this.getAttribute('hash')
    const target = this.shadowRoot.getElementById(hash)
    if (target) {
      window.scrollTo(0, target.offsetTop)
    }
  }
}
