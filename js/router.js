'use strict'

class Router {
  constructor (options) {
    this.routes = []
    const defaults = {'default': '#'}
    this.options = Object.assign({}, defaults, options)
    this.updatePath(location.hash.slice(1))
  }

  updatePath (url) {
    const full = url.split('#')
    const [pathname, params] = full[0].split('?')
    this.currentPath = full[0]
    this.currentPathName = pathname
    this.queryParams = new URLSearchParams(params)
    this.currentHash = full[1]
  }

  add (route, func) {
    this.routes.push({route: route, func: func})
    return this
  }

  add404 (func) {
    this.error404 = func
    return this
  }

  redirect (route) {
    location.href = route || this.options.default
    return this
  }

  goto (route) {
    this.updatePath(route)
    const match = this.routes.find(item => item.route === this.currentPathName)
    if (!match) {
      return this.error404 ? this.error404() : this.redirect()
    }
    return match.func && match.func(this.queryParams)
  }

  listen (callback) {
    window.addEventListener('hashchange', event => {
      const resp = router.goto(location.hash.slice(1) || this.options.default)
      callback && callback(resp)
    })
    return this
  }

  start (callback) {
    if (!this.currentPath) {
      this.redirect(this.options.default)
    }
    callback && callback()
    router.goto(this.currentPath)
  }
}
